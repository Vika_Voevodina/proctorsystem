#include <drogon/drogon.h>
#include <drogon/HttpAppFramework.h>
#include <controllers/TestCtrl.h>

using namespace drogon;

int main() {
  std::vector<std::string> viewsPath = {"../views"};
  HttpAppFramework &enableDynamicViewsLoading(const std::vector< std::string > &viewsPath);

  app()
      .addListener("0.0.0.0", 80)
      .loadConfigFile("../config.json")
      .setStaticFilesCacheTime(500)
      .registerHandler("/",
                       [ ](const HttpRequestPtr& req,
                          std::function<void (const HttpResponsePtr &)> &&callback)
                       {
                         auto resp=HttpResponse::newRedirectionResponse("/TestCtrl/");
                         callback(resp);
                       })
      .run();
  return 0;
}