@echo off
mkdir includes
cd includes
git clone https://github.com/microsoft/vcpkg
call vcpkg\bootstrap-vcpkg.bat
cd includes
FOR /f "tokens=2 delims==" %%f IN ('wmic os get osarchitecture /value ^| find "="') DO SET "OS_ARCH=%%f"
IF "%OS_ARCH%"=="32-bit" GOTO :32bit
IF "%OS_ARCH%"=="64-bit" GOTO :64bit

ECHO "OS Architecture %OS_ARCH% is not supported!"
REM EXIT 1

:32bit
ECHO "32 bit Operating System"
vcpkg\vcpkg.exe install drogon --triplet x86-windows
GOTO :SUCCESS

:64bit
ECHO "64 bit Operating System"
vcpkg\vcpkg.exe install drogon --triplet x64-windows
GOTO :SUCCESS

:SUCCESS
vcpkg\vcpkg.exe integrate install
cd ..
echo "Success!"