#include "TestCtrl.h"
#include <string>
#include <cctype>
#include <sstream>


char from_hex(char ch) {
  return isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10;
}

std::string url_decode(std::string text) {
  char h;
  std::ostringstream escaped;
  escaped.fill('0');

  for (auto i = text.begin(), n = text.end(); i != n; ++i) {
    std::string::value_type c = (*i);

    if (c == '%') {
      if (i[1] && i[2]) {
        h = from_hex(i[1]) << 4 | from_hex(i[2]);
        escaped << h;
        i += 2;
      }
    } else if (c == '+') {
      escaped << ' ';
    } else {
      escaped << c;
    }
  }
  return escaped.str();
}


void TestCtrl::getForm(const HttpRequestPtr &req, std::function<void(const HttpResponsePtr &)> &&callback) const {
  HttpViewData data;
  auto resp = HttpResponse::newHttpViewResponse("New.csp", data);
  callback(resp);
}

void TestCtrl::list(const HttpRequestPtr &req, std::function<void(const HttpResponsePtr &)> &&callback) const {
  HttpViewData data;
  data.insert("title", "Список для прокторов");

  auto dbc = drogon::app().getDbClient("my_client");
  dbc->execSqlSync("create table if not exists ad(id int primary key, phone text, time text, university text)");
  auto res = dbc->execSqlSync("SELECT time, phone, university from ad");
  std::vector<std::map<std::string, std::string>> db_data;
  std::map<std::string, std::string> dict_row = {};
  for (auto row : res) {
    dict_row = {
        {"university", row["university"].as<std::string>()},
        {"time",       row["time"].as<std::string>()},
        {"phone",      row["phone"].as<std::string>()},
    };
    db_data.push_back(dict_row);
  }
  data.insert("db_data", db_data);
  auto resp = HttpResponse::newHttpViewResponse("List.csp", data);
  callback(resp);
}

void TestCtrl::list_delete(const HttpRequestPtr &req, std::function<void(const HttpResponsePtr &)> &&callback) const {
  HttpViewData data;
  data.insert("title", "Список для прокторов");

  auto dbc = drogon::app().getDbClient("my_client");
  dbc->execSqlSync("create table if not exists ad(id int primary key, phone text, time text, university text)");
  auto res = dbc->execSqlSync("SELECT time, phone, university from ad");
  std::vector<std::map<std::string, std::string>> db_data;
  std::map<std::string, std::string> dict_row = {};
  for (auto row : res) {
    dict_row = {
        {"university", row["university"].as<std::string>()},
        {"time",       row["time"].as<std::string>()},
        {"phone",      row["phone"].as<std::string>()},
    };
    db_data.push_back(dict_row);
  }
  data.insert("db_data", db_data);
  auto resp = HttpResponse::newHttpViewResponse("ListDel.csp", data);
  callback(resp);
}


std::map<std::string, std::string> decodeForm(const char *body) {
  std::map<std::string, std::string> decoded;
  std::string key, value;
  bool isKey(true);

  for (char sym : std::string(body)) {
//    std::cout << sym << std::endl;
    if (sym == '=') {
      isKey = false;
    } else if (sym == '&') {
      isKey = true;
      std::cout << key << " " << url_decode(value) << std::endl;
      decoded[key] = url_decode(value);
      key = "";
      value = "";
    } else {
      if (isKey) {
        key += sym;
      } else {
        value += sym;
      }
    }
  }
  decoded[key] = value;
  return decoded;
}

void TestCtrl::insert(const HttpRequestPtr &req, std::function<void(const HttpResponsePtr &)> &&callback) const {
  auto data = decodeForm(req->bodyData());
  auto dbc = drogon::app().getDbClient("my_client");
  dbc->execSqlSync("create table if not exists ad(id int primary key, phone text, time text, university text)");
  std::string sqlStr = "insert into ad (phone, time, university) values (\"" +
                       data["phone"] + "\", \"" + data["time"] + "\", \"" + data["university"] +"\")";
  std::cout << "SQL: " << sqlStr;
  dbc->execSqlSync(sqlStr);
  std::cout << "Inserted: " << data["university"] << " " << data["time"] << " " << data["phone"] << std::endl;
  auto resp = HttpResponse::newRedirectionResponse("/TestCtrl/list", k302Found);
  callback(resp);
}