#pragma once

#include <drogon/HttpController.h>
#include "models/Ad.h"
using namespace drogon;
using namespace drogon_model::sqlite3;
class TestCtrl : public drogon::HttpController<TestCtrl> {
public:
  METHOD_LIST_BEGIN
//    METHOD_ADD(TestCtrl::login, "/token?userId={1}&passwd={2}", Post);
//    METHOD_ADD(TestCtrl::getInfo, "/list_para", Get);
    METHOD_ADD(TestCtrl::getForm, "/", Get);
    METHOD_ADD(TestCtrl::list, "/list", Get);
    METHOD_ADD(TestCtrl::list_delete, "/list/delete", Get);
    METHOD_ADD(TestCtrl::insert, "/", Post);
  METHOD_LIST_END


  void getForm(const HttpRequestPtr &req,
               std::function<void(const HttpResponsePtr &)> &&callback) const;


  void list(const HttpRequestPtr &req, std::function<void(const HttpResponsePtr &)> &&callback) const;
  void list_delete(const HttpRequestPtr &req, std::function<void(const HttpResponsePtr &)> &&callback) const;

  void insert(const HttpRequestPtr &req, std::function<void(const HttpResponsePtr &)> &&callback) const;

};
